package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

public class SoapClientTest {

	public static void main(String[] args) {
		
		EmployeeManagementWebService service = new EmployeeManagementWebServiceImplementationService().getEmployeeManagementWebServicePort();

		
		List <Employee> list = service.getAllEmployees();
		
		for (Employee emp : list) {
			System.out.println(emp.getId() + " " + emp.getFirstName() + " " + emp.getSurName());
		}
//		
//		Employee employee = service.getEmployeeById(102);
//		
//		System.out.println(employee.getId() + " " + employee.getFirstName() + " " + employee.getSurName());
		
		Employee employee = new Employee();
		employee.setFirstName("Boog");
		employee.setSurName("Grizzly");
		employee.setJobRole("Bear");
		employee.setSalary(125);
		
		try {
			service.registerEmployee(employee);
		} catch (ServiceUnavailableException_Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
